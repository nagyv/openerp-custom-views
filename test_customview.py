from osv import osv

class res_partner(osv.osv):
	_inherit = 'res.partner'
	def say_hello(self, cr, uid, message):
		return {'response': message}