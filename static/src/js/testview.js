openerp.test_customview = function(openerp) {
var QWeb = openerp.web.qweb;

/** @namespace */
openerp.test_customview = openerp.test_customview || {};

/*
 * Widgets
 * This client action designed to be used as a dashboard widget display
 * the html content of a res_widget given as argument
 */
openerp.web.client_actions.add( 'test.custom.view', 'openerp.test_customview.Widget');
openerp.test_customview.Widget = openerp.web.View.extend(/** @lends openerp.test_customview.Widgets# */{
    template: 'CustomWidget',
    /**
     * Initializes a "HomeWidget" client widget: handles the display of a given
     * res.widget objects in an OpenERP view (mainly a dashboard).
     *
     * @constructs openerp.test_customview.Widget
     * @extends openerp.web.View
     *
     * @param {Object} parent
     * @param {Object} options
     * @param {Number} options.widget_id
     */
    init: function (parent, options) {
        this._super(parent);
        this.widget_id = options.widget_id;
    },
    start: function () {
        this._super();
        var param = {"model": 'res.partner', "method": "say_hello", args: ['Hello']};
        return this.rpc('/web/dataset/call', param).then(this.on_widget_loaded);
    },
    on_widget_loaded: function (widget) {
        this.$element.html(QWeb.render('CustomWidget.content', {
            response: widget.response
        }));
    }
});

};
