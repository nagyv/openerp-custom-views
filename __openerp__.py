# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-TODAY OpenERP S.A. <http://www.openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Custom Web View',
    'version': '1.0',
    'category': 'Tools',
    'description': """
This module is supposed to demonstrate the implementation of an arbitrary custom view.

It adds a new menu item under Settings/Customizations that shows a simple string sent to the server and passed back to the client.
    """,
    'author': 'Viktor Nagy',
    'depends': ['board'],
    'update_xml': ['custom_view.xml'],
    'demo_xml': [],
    'test': [],
    'js': ['static/src/js/testview.js'],
    'css': [],
    'qweb': ["static/src/xml/*.xml"],
    'installable': True,
    'auto_install': False
}
